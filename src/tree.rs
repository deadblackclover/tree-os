// Copyright (c) 2020, DEADBLACKCLOVER.

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
use vga::colors::Color16;
use vga::writers::Graphics640x480x16;
use vga_figures::figures2d::Figures2D;

pub const WIDTH: isize = 640;
pub const HEIGHT: isize = 480;

pub fn happy_new_year(figures: &Figures2D<Graphics640x480x16>, x: usize, y: usize, color: Color16) {
    figures.text(x - 55, y, "Happy New Year!", color);
}

pub fn tree(figures: &Figures2D<Graphics640x480x16>, x: isize, y: isize) {
    let arr = [
        x,
        y,
        (x - 50),
        (y + 50),
        (x - 10),
        (y + 50),
        (x - 75),
        (y + 100),
        (x - 10),
        (y + 100),
        (x - 100),
        (y + 150),
        (x + 100),
        (y + 150),
        (x + 10),
        (y + 100),
        (x + 75),
        (y + 100),
        (x + 10),
        (y + 50),
        (x + 50),
        (y + 50),
        x,
        y,
    ];

    figures.rectangle(x - 10, y + 150, x + 10, y + 180, Color16::Brown);
    figures.polygon(&arr, Color16::Green);
}

pub fn toy(figures: &Figures2D<Graphics640x480x16>, x: isize, y: isize, color: Color16) {
    figures.ellipse(x, y, 10, 10, color);
}
